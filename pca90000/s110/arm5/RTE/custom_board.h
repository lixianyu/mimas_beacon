#ifndef CUSTOM_BOARD_H
#define CUSTOM_BOARD_H

#ifdef AKII
// LEDs definitions for CUSTOM_BOARD AK-II
#define LEDS_NUMBER    5

#define LED_START      18
#define LED_1          18
#define LED_2          19
#define LED_3          20
#define LED_4          21
#define LED_5          22
#define LED_STOP       22

#define LEDS_LIST { LED_1, LED_2, LED_3, LED_4, LED_5 }

#define BSP_LED_0      LED_1
#define BSP_LED_1      LED_2
#define BSP_LED_2      LED_3
#define BSP_LED_3      LED_4
#define BSP_LED_4      LED_5

#define BSP_LED_0_MASK (1<<BSP_LED_0)
#define BSP_LED_1_MASK (1<<BSP_LED_1)
#define BSP_LED_2_MASK (1<<BSP_LED_2)
#define BSP_LED_3_MASK (1<<BSP_LED_3)
#define BSP_LED_4_MASK (1<<BSP_LED_4)

#define LEDS_MASK      (BSP_LED_0_MASK | BSP_LED_1_MASK | BSP_LED_2_MASK | BSP_LED_3_MASK | BSP_LED_4_MASK)
/* all LEDs are lit when GPIO is low */
#define LEDS_INV_MASK  (~(LEDS_MASK))
////////////////////////////////////////////////////////////////////////////////////////////////////

#define BUTTONS_NUMBER 2

#define BUTTON_START   16
#define BUTTON_1       16
#define BUTTON_2       17
#define BUTTON_STOP    17
#define BUTTON_PULL    NRF_GPIO_PIN_PULLUP

#define BUTTONS_LIST { BUTTON_1, BUTTON_2 }

#define BSP_BUTTON_0   BUTTON_1
#define BSP_BUTTON_1   BUTTON_2

#define BSP_BUTTON_0_MASK (1<<BSP_BUTTON_0)
#define BSP_BUTTON_1_MASK (1<<BSP_BUTTON_1)

#define BUTTONS_MASK   0x00030000
#if 0
#define RX_PIN_NUMBER  14
#define TX_PIN_NUMBER  12
#define CTS_PIN_NUMBER 10
#define RTS_PIN_NUMBER 8
#define HWFC           false
#else
#define RX_PIN_NUMBER  8
#define TX_PIN_NUMBER  9
#define CTS_PIN_NUMBER 10
#define RTS_PIN_NUMBER 11
#define HWFC           false
#endif
#else // Not defined AKII
///////////////////////////////////////////////////////////////////////////
// LEDs definitions for CUSTOM_BOARD Toweer
#if 0
#define LEDS_NUMBER    2

#define LED_START      4
#define LED_1          4
#define LED_2          5
#define LED_STOP       5

#define LEDS_LIST { LED_1, LED_2 }

#define BSP_LED_0      LED_1
#define BSP_LED_1      LED_2
#define BSP_LED_0_MASK (1<<BSP_LED_0)
#define BSP_LED_1_MASK (1<<BSP_LED_1)

#define LEDS_MASK      (BSP_LED_0_MASK|BSP_LED_1_MASK)
/* all LEDs are lit when GPIO is low */
#define LEDS_INV_MASK  (~(LEDS_MASK))
#else
//.................................................................................................
#define LEDS_NUMBER    1
#ifdef MIMAS_VER3
#define LED_START      17
#define LED_1          17
#define LED_STOP       17
#else
#ifdef MIMAS_VER2
#define LED_START      24
#define LED_1          24
#define LED_STOP       24
#else
#define LED_START      4
#define LED_1          4
#define LED_STOP       4
#endif // MIMAS_VER2
#endif // MIMAS_VER3
#define LEDS_LIST { LED_1 }
#define BSP_LED_0      LED_1
#define BSP_LED_0_MASK (1<<BSP_LED_0)

#define LEDS_MASK      (BSP_LED_0_MASK)
/* all LEDs are lit when GPIO is low */
#define LEDS_INV_MASK  (~(LEDS_MASK))

#ifndef MIMAS_VER2
#ifndef MIMAS_VER3
//当电量低时使用P0.04脚，因为该脚没有电阻
#define LED_BACKUP    5
#endif
#endif
#endif

////////////////////////////////////////////////////////////////////////////////////////////////////

#define BUTTONS_NUMBER 1
#ifdef MIMAS_VER3
#define BUTTON_START   24
#define BUTTON_1       24
#define BUTTON_STOP    24
#else
#ifdef MIMAS_VER2
#define BUTTON_START   5
#define BUTTON_1       5
#define BUTTON_STOP    5
#else
#define BUTTON_START   17
#define BUTTON_1       17
#define BUTTON_STOP    17
#endif
#endif // MIMAS_VER3

#define BUTTON_PULL    NRF_GPIO_PIN_PULLUP

#define BUTTONS_LIST { BUTTON_1 }

#define BSP_BUTTON_0   BUTTON_1

#define BSP_BUTTON_0_MASK (1<<BSP_BUTTON_0)

#define BUTTONS_MASK   0x00020000

#ifdef MIMAS_VER3
#define RX_PIN_NUMBER  23
#else
#ifdef MIMAS_VER2
#define RX_PIN_NUMBER  23
#else
#define RX_PIN_NUMBER  24
#endif
#endif // MIMAS_VER3
#define TX_PIN_NUMBER  25
#define CTS_PIN_NUMBER 22
#define RTS_PIN_NUMBER 21
#define HWFC           false

#ifdef MIMAS_VER3
#define BUZZER_OUT 5
#else
#ifdef MIMAS_VER2
#define BUZZER_OUT 17
#endif
#endif // MIMAS_VER3
#endif //#ifdef AKII
////////////////////////////////////////////////////////////////////////////////////////////////////
#define SPIM0_SCK_PIN       13     /**< SPI clock GPIO pin number. */
#define SPIM0_MOSI_PIN      12     /**< SPI Master Out Slave In GPIO pin number. */
#define SPIM0_MISO_PIN      11     /**< SPI Master In Slave Out GPIO pin number. */
#define SPIM0_SS_PIN        10     /**< SPI Slave Select GPIO pin number. */


#endif // CUSTOM_BOARD_H
