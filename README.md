基于nRF51 SDK v10.0.0；
Base ble_app_proximity
----------------------------------------------------------------------------------------------------
If there is a bootloader:
bootloader start address : 3C000, and size is 16KB
// swap
nrfjprog --memrd 0x0003BC00 --n 1024

// g_storage_handle_temperature
nrfjprog --memrd 0x0003B800 --n 1024

// g_storage_handle
nrfjprog --memrd 0x0003B400 --n 1024

// Device manager: m_storage_handle
nrfjprog --memrd 0x0003B000 --n 1024

----------------------------------------------------------------------------------------------------

nrfutil dfu genpkg --application Mimas_nrf51822_xxaa_s110.hex --application-version 1 Mimas_0.1.0801.01.zip
nrfutil dfu genpkg --application Mimas_nrf51822_xxaa_s110.hex --application-version 2 Mimas_0.1.1016.00.zip
nrfutil dfu genpkg --application Mimas_nrf51822_xxaa_s110.hex --application-version 3 Mimas_0.1.1016.01.zip
nrfutil dfu genpkg --application Mimas_nrf51822_xxaa_s110.hex --application-version 4 Mimas_0.1.1017.00.zip
nrfutil dfu genpkg --application Mimas_nrf51822_xxaa_s110.hex --application-version 5 Mimas_0.1.1018.00.zip