#ifndef MIMAS_CONFIG_H__
#define MIMAS_CONFIG_H__

#define BLOCK_SIZE 32
#define BLOCK_COUNT 32

#define BLOCK_NUM_PROFILE 0
#define BLOCK_NUM_STEP_TARGET 1
#define BLOCK_NUM_OS 2
#define BLOCK_NUM_SN 3
#define BLOCK_NUM_TEST_FLAG 4
#define BLOCK_NUM_USER_ID 5

#define BLOCK_NUM_MAJOR 6
#define BLOCK_NUM_MINOR 7
#define BLOCK_NUM_Calibrated_Tx_Power 8
#define BLOCK_NUM_DEVICE_TX_POWER 9
#define BLOCK_NUM_ADV_Freq 10 //Unit: 100ms, 1~102.
#define BLOCK_NUM_UUID 11
#define BLOCK_NUM_TEMP_OFFSET_COUNT 12
#define BLOCK_NUM_PEI_DUI 13
#define BLOCK_NUM_ZHU_CE 14
#define BLOCK_NUM_FACTORY 15

///////////////////////////////////////
#define PSTORAGE_STATE_UPDATE_IDLE 0
#define PSTORAGE_STATE_UPDATE_UUID 1
#define PSTORAGE_STATE_UPDATE_MAJOR 2
#define PSTORAGE_STATE_UPDATE_MINOR 3 
#define PSTORAGE_STATE_UPDATE_CALIBRATED_TX_POWER 4 
#define PSTORAGE_STATE_UPDATE_ADV_INTERVAL 5
#define PSTORAGE_STATE_UPDATE_TX_POWER 6
#define PSTORAGE_STATE_UPDATE_TEMPERATURE_CALIBRATE 7
#define PSTORAGE_STATE_UPDATE_PEIDUI_PASSWORD 8
#define PSTORAGE_SAVE_BANGDING 9
#define PSTORAGE_DELETE_BANGDING_ID 10
#define PSTORAGE_SAVE_FACTORY 11
///////////////////////////////////////


/* Bit 0 : active or inactive. */
#define ACTIVE_INACTIVE_Pos ((uint8_t)0) /*!< active or inactive field. */
#define ACTIVE_INACTIVE_Msk ((uint8_t)0x1 << ACTIVE_INACTIVE_Pos) /*!< Bit mask of active or inactive. */
#define ACTIVE_INACTIVE_0 ((uint8_t)0) /*!< inactive. */
#define ACTIVE_INACTIVE_1 ((uint8_t)1) /*!< active. */

/* Bits 2..1 :  obfuscating algorithms. */
#define OBFUSCATING_ALGORITHMS_Pos ((uint8_t)1) /*!< Position of obfuscating algorithms field. */
#define OBFUSCATING_ALGORITHMS_Msk ((uint8_t)0x3 << OBFUSCATING_ALGORITHMS_Pos) /*!< Bit mask of obfuscating algorithms field. */
#define OBFUSCATING_ALGORITHMS_0 ((uint8_t)0x00) /*!< No obfuscating algorithms. */
#define OBFUSCATING_ALGORITHMS_1 ((uint8_t)0x01) /*!< obfuscating algorithms 1. */
#define OBFUSCATING_ALGORITHMS_2 ((uint8_t)0x02) /*!< obfuscating algorithms 2. */
#define OBFUSCATING_ALGORITHMS_3 ((uint8_t)0x03) /*!< obfuscating algorithms 3. */


#ifdef TETHYS_PAIR
extern uint8_t gPasskey[6];
#endif
#endif  //MIMAS_CONFIG_H__
