@echo off
echo.
echo 擦除flash...
nrfjprog -e

echo.
echo 烧写协议栈...
nrfjprog --program softdevice.hex

echo.
echo 烧写软件...
nrfjprog --program Mimas_nrf51822_xxaa_s110.hex

echo.
echo 烧写bootloader
nrfjprog --program nrf51422_xxac_new1_bootloader.hex
nrfjprog --memwr 0x0003FC00 --val 0x01


goto END



echo.
echo 写入ADXL362工作模式
nrfjprog --memwr 0x100010A4 --val 0x00000000

echo 写入major
nrfjprog --memwr 0x10001080 --val %1
nrfjprog --memwr 0x10001084 --val 0xFFFFFFFE

echo.
echo 写入minor
nrfjprog --memwr 0x10001088 --val %2
nrfjprog --memwr 0x1000108C --val 0xFFFFFFFE

nrfjprog --memwr 0x10001090 --val 0x6f8f0aa4
nrfjprog --memwr 0x10001094 --val 0x3b194be7
nrfjprog --memwr 0x10001098 --val 0x9510e62b
nrfjprog --memwr 0x1000109C --val 0xf19fecea
nrfjprog --memwr 0x100010A0 --val 0xFFFFFFFE

:END
echo.
echo 系统重启
nrfjprog -r

:ERROR
echo.